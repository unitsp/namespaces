 <?php

use App\WebObject\Library;
use App\WebObject\BookLibrary as OLOLO;
use App\WebObject\BookLibrary\Book as B;

echo B::page();
echo "<br>";
echo Library\MyClass::method();

echo "<br>";
echo \App\WebObject\User::getName();

function __autoload($class){
	$path = 'Classes/'.str_replace('\\', '/', $class);
	$path = $path.'.php';
	require_once($path);
}